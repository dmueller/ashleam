from ashleam.numba_target import myjit, my_parallel_loop
import ashleam.lattice as l
import ashleam.su as su


def evolve(s, stream=None):
    # Standard way to 'cast' numpy arrays from python objects
    # cdef cnp.ndarray[double, ndim=1, mode="c"] array = s.array

    u0 = s.d_u0
    u1 = s.d_u1
    et1 = s.d_et1
    et0 = s.d_et0
    phi0 = s.d_phi0
    phi1 = s.d_phi1
    eta1 = s.d_eta1
    eta0 = s.d_eta0

    dt = s.dt
    n = s.n

    my_parallel_loop(evolve_kernel, n * n, u0, u1, et1, et0, phi1, phi0, eta1, eta0, dt, n, stream=stream)


@myjit
def evolve_kernel(xi, u0, u1, et1, et0, phi1, phi0, eta1, eta0, dt, n):
    # Momentum update
    # Input:
    #   t-dt/2: pt0, peta0
    #   t: u0, aeta0
    #
    # Output:
    #   t+dt/2: pt1, peta1
    #   t+dt: u1, aeta1

    peta_local = su.load(eta0[xi])

    for d in range(2):
        # transverse electric field update
        buffer2 = l.plaquettes(xi, d, u0, n)
        b2 = l.add_mul(et0[xi, d], buffer2, - dt)
        buffer1 = l.transport(phi0, u0, xi, d, 1, n)
        buffer2 = l.comm(buffer1, phi0[xi])
        b2 = l.add_mul(b2, buffer2, + dt)
        su.store(et1[xi, d], b2)

        # longitudinal electric field update
        buffer1 = l.transport(phi0, u0, xi, d, 1, n)
        buffer2 = l.transport(phi0, u0, xi, d, -1, n)
        buffer1 = su.add(buffer1, buffer2)
        buffer1 = l.add_mul(buffer1, phi0[xi], -2)
        peta_local = l.add_mul(peta_local, buffer1, + dt)

    su.store(eta1[xi], peta_local)

    # Coordinate update
    for d in range(2):
        # transverse link variables update
        buffer0 = su.mul_s(et1[xi, d], dt)
        buffer1 = su.mexp(buffer0)
        buffer2 = su.mul(buffer1, u0[xi, d])
        su.store(u1[xi, d], buffer2)

    # longitudinal gauge field update
    b2 = l.add_mul(phi0[xi], eta1[xi], dt)
    su.store(phi1[xi], b2)


@myjit
def gauss_kernel(xi, u0, u1, et1, et0, phi1, phi0, eta1, eta0, n, gauss_sq):
    # transverse contribution
    local_gauss = su.zero()
    for d in range(2):
        xs = l.shift(xi, d, -1, n)
        diff_E = l.add_mul(et1[xi, d], l.act(u0[xs, d], et1[xs, d]), -1)
        local_gauss = l.add_mul(local_gauss, diff_E, +1)

    # longitudinal contribution
    local_gauss = l.add_mul(local_gauss, l.comm(phi0[xi], eta1[xi]), +1)

    # take square
    local_gauss_sq = su.sq(local_gauss)

    # store in array
    gauss_sq[xi] += local_gauss_sq


