import numpy as np
from ashleam.numba_target import use_cuda
if use_cuda:
    import numba.cuda as cuda
import ashleam.leapfrog as leapfrog
import ashleam.su as su
import ashleam.lattice as l
from ashleam.numba_target import myjit, my_parallel_loop


class Simulation:
    def __init__(self, n, dt):
        # basic parameters
        self.n = n
        self.dt = dt
        nn = self.n ** 2

        # fields (times after evolve())
        self.u0 = np.zeros((nn, 2, su.GROUP_ELEMENTS), dtype=su.GROUP_TYPE) # U_{x,i}(t_n)
        self.u1 = np.zeros((nn, 2, su.GROUP_ELEMENTS), dtype=su.GROUP_TYPE) # U_{x,i}(t_(n+1))
        self.et1 = np.zeros((nn, 2, su.GROUP_ELEMENTS), dtype=su.GROUP_TYPE) # E_i(t_(n+1/2))
        self.et0 = np.zeros((nn, 2, su.GROUP_ELEMENTS), dtype=su.GROUP_TYPE) # E_i(t_(n-1/2))

        self.phi0 = np.zeros((nn, su.GROUP_ELEMENTS), dtype=su.GROUP_TYPE) # phi(t_n)
        self.phi1 = np.zeros((nn, su.GROUP_ELEMENTS), dtype=su.GROUP_TYPE) # phi(t_(n+1))
        self.eta1 = np.zeros((nn, su.GROUP_ELEMENTS), dtype=su.GROUP_TYPE) # \eta(t_(n+1/2))
        self.eta0 = np.zeros((nn, su.GROUP_ELEMENTS), dtype=su.GROUP_TYPE) # \eta(t_(n-1/2))

        self.data = [self.u0, self.u1, self.et1, self.et0, self.phi0, self.phi1, self.eta1, self.eta0]

        self.t = 0.0

        # Memory on the device:
        # - on CPU: contains pointer to Numpy array
        # - for CUDA: contains pointer to device (GPU) memory
        self.d_u0 = self.u0
        self.d_u1 = self.u1
        self.d_et1 = self.et1
        self.d_et0 = self.et0
        self.d_phi0 = self.phi0
        self.d_phi1 = self.phi1
        self.d_eta1 = self.eta1
        self.d_eta0 = self.eta0

        # initialize fields
        self.copy_to_device()
        self.init()
        self.copy_to_host()

    def swap(self):
        self.eta1, self.eta0 = self.eta0, self.eta1
        self.et1, self.et0 = self.et0, self.et1
        self.u1, self.u0 = self.u0, self.u1
        self.phi1, self.phi0 = self.phi0, self.phi1

        # Also swap pointers to CUDA device memory
        self.d_eta1, self.d_eta0 = self.d_eta0, self.d_eta1
        self.d_et1, self.d_et0 = self.d_et0, self.d_et1
        self.d_u1, self.d_u0 = self.d_u0, self.d_u1
        self.d_phi1, self.d_phi0 = self.d_phi0, self.d_phi1

    def get_ngb(self):
        nbytes = 0
        for d in self.data:
            nbytes += d.nbytes
        return nbytes / 1024.0 ** 3

    def copy_to_device(self):
        if use_cuda:
            self.d_u0 = cuda.to_device(self.u0)
            self.d_u1 = cuda.to_device(self.u1)
            self.d_et1 = cuda.to_device(self.et1)
            self.d_et0 = cuda.to_device(self.et0)
            self.d_phi0 = cuda.to_device(self.phi0)
            self.d_phi1 = cuda.to_device(self.phi1)
            self.d_eta1 = cuda.to_device(self.eta1)
            self.d_eta0 = cuda.to_device(self.eta0)

    def copy_to_host(self):
        if use_cuda:
            self.d_u0.copy_to_host(self.u0)
            self.d_u1.copy_to_host(self.u1)
            self.d_et1.copy_to_host(self.et1)
            self.d_et0.copy_to_host(self.et0)
            self.d_phi0.copy_to_host(self.phi0)
            self.d_phi1.copy_to_host(self.phi1)
            self.d_eta1.copy_to_host(self.eta1)
            self.d_eta0.copy_to_host(self.eta0)

    def evolve_step(self):
        self.swap()
        self.t += self.dt
        leapfrog.evolve(self, None)

    def init(self):
        my_parallel_loop(init_kernel, self.n * self.n, self.d_u0, self.d_u1, self.d_et1, self.d_et0,
                         self.d_phi1, self.d_phi0, self.d_eta1, self.d_eta0)

    def energy(self):
        ecomp = np.zeros(shape=(self.n * self.n, 4), dtype=np.double)

        if use_cuda:
            d_e_components = cuda.to_device(ecomp)
        else:
            d_e_components = ecomp

        my_parallel_loop(energy_kernel, self.n * self.n, self.d_u0, self.d_u1, self.d_et1, self.d_et0,
                         self.d_phi1, self.d_phi0, self.d_eta1, self.d_eta0, self.n, d_e_components)

        if use_cuda:
            d_e_components.copy_to_host(ecomp)

        eET, eEL = ecomp[:, 0].reshape(self.n, self.n), ecomp[:, 1].reshape(self.n, self.n)
        eBT, eBL = ecomp[:, 2].reshape(self.n, self.n), ecomp[:, 3].reshape(self.n, self.n)

        return eET, eEL, eBT, eBL

    def gauss_sq(self):
        gauss_density_sq = np.zeros(shape=(self.n * self.n), dtype=np.double)

        if use_cuda:
            d_gauss_density_sq = cuda.to_device(gauss_density_sq)
        else:
            d_gauss_density_sq = gauss_density_sq

        my_parallel_loop(leapfrog.gauss_kernel, self.n * self.n, self.d_u0, self.d_u1, self.d_et1, self.d_et0,
                         self.d_phi1, self.d_phi0, self.d_eta1, self.d_eta0, self.n, d_gauss_density_sq)

        if use_cuda:
            d_gauss_density_sq.copy_to_host(gauss_density_sq)

        return gauss_density_sq


@myjit
def init_kernel(xi, u0, u1, et1, et0, phi1, phi0, eta1, eta0):
    # set all transverse links to unit matrices
    for d in range(2):
        su.store(u0[xi, d], su.unit())
        su.store(u1[xi, d], su.unit())

    # set longitudinal gauge fields to zero
    su.store(phi0[xi], su.zero())
    su.store(phi1[xi], su.zero())

    # set all transverse electric fields to zero
    for d in range(2):
        su.store(et0[xi, d], su.zero())
        su.store(et1[xi, d], su.zero())

    # set longitudinal electric fields to zero
    su.store(eta0[xi], su.zero())
    su.store(eta1[xi], su.zero())


@myjit
def energy_kernel(xi, u0, u1, et1, et0, phi1, phi0, eta1, eta0, n, ecomp):
    # electric transverse
    for d in range(2):
        ecomp[xi, 0] += 0.5 * (su.sq(et0[xi, d]) + su.sq(et1[xi, d]))

    # electric longitudinal
    ecomp[xi, 1] += 0.5 * (su.sq(eta0[xi]) + su.sq(eta1[xi]))

    # magnetic transverse
    for d in range(2):
        phi_trans = l.transport(phi0, u0, xi, d, 1, n)
        phi_local = phi0[xi]
        diff_phi = l.add_mul(phi_trans, phi_local, -1)
        ecomp[xi, 2] += su.sq(diff_phi)

    # magnetic longitudinal
    plaq_ah = su.ah(l.plaq(u0, xi, 0, 1, +1, +1, n))
    ecomp[xi, 3] += su.sq(plaq_ah)





