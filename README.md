# ashleam

Ashleam bay is a very nice place in Achill Island, Ireland. It is also the name of this code, which simulates the
effective 2+1 dimensional Yang-Mills equations using lattice gauge theory and a leapfrog time stepper. It is exactly
invariant under lattice gauge transformations and, as a result, conserves the Gauss constraint up to machine precision.

Parts of this code are taken from [curraun](https://gitlab.com/openpixi/curraun).

Features:
* Standard leapfrog solver in terms of link variables and color-electric fields for SU(2) and SU(3)
* Initial conditions for color-electric flux tubes
* Basic energy density computation
* Gauss constraint computation

## Theory
The [`notes`](notes) folder contains a derivation of the equations of motion.

## Setup
You can install all dependencies with [Anaconda](https://www.anaconda.com/distribution/) by creating the conda
environment described in `env.yml`. 

To create it, simply run
```shell
conda env create -f env.yml
```

## Examples
Jupyter notebooks are provided in the [`notebooks`](notebooks) folder.

You can start a Jupyter notebook server via


```shell
conda activate ashleam
jupyter notebook
```